using FluentValidation;
using Payment.API.Models.DTOs.Requests;

namespace Payment.API.Validation;

public class SaleRequestDTOValidation : AbstractValidator<SaleRequestDTO>
{
    public SaleRequestDTOValidation()
    {
        RuleFor(e => e.Items).NotNull().NotEmpty();
        RuleFor(e => e.Salesperson).NotNull().SetValidator(new SalespersonRequestDTOValidation());
        RuleFor(e => e.Date).NotEmpty();             
    }
}