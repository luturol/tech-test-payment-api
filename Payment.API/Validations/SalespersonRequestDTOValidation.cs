using FluentValidation;
using Payment.API.Models.DTOs.Requests;

namespace Payment.API.Validation;

public class SalespersonRequestDTOValidation : AbstractValidator<SalespersonRequestDTO>
{
    public SalespersonRequestDTOValidation()
    {
        RuleFor(e => e.CPF).NotNull().NotEmpty().Length(11);
        RuleFor(e => e.Name).NotEmpty();
        RuleFor(e => e.Email).NotEmpty().EmailAddress();
        RuleFor(e => e.Phone).MinimumLength(9);
    }
}