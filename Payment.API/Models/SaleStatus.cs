namespace Payment.API.Models;

public enum SaleStatus
{
    PaymentApproved,
    SentToCarrier,
    Delivered,
    Canceled,
    WaitingForPayment
}