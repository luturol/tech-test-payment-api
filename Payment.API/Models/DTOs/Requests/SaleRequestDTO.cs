namespace Payment.API.Models.DTOs.Requests;

public record SaleRequestDTO
{
    public List<ItemRequestDTO> Items { get; set; }
    public SalespersonRequestDTO Salesperson { get; set; }
    public DateTime Date { get; set; }    
}