namespace Payment.API.Models.DTOs.Requests;

public record ItemRequestDTO
{
    public string Name { get; set; }
    public int Price { get; set; }
}