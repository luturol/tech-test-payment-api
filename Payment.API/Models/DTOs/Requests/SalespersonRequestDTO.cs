namespace Payment.API.Models.DTOs.Requests;

public record SalespersonRequestDTO
{
   public string CPF { get; set; }
    public string Email { get; set; }    
    public string Name { get; set; }
    public string Phone { get; set; }
}