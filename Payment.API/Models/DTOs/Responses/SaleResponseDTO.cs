using Payment.API.Models.DTOs.Response;

namespace Payment.API.Models.DTOs.Requests;

public record SaleResponseDTO
{
    public DateTime Date { get; set; }    
    public Guid Id {get; set;}
    public List<ItemResponsetDTO> Items { get; set; }
    public SalespersonResponseDTO Salesperson { get; set; }    
    public SaleStatus Status { get; set; }
}