namespace Payment.API.Models.DTOs.Response;

public record ItemResponsetDTO
{
    public string Name { get; set; }
    public int Price { get; set; }
}