namespace Payment.API.Models;

public class Salesperson
{    
    public string CPF { get; set; }
    public string Email { get; set; }
    public Guid Id { get; set; }
    public string Name { get; set; }
    public string Phone { get; set; }
}