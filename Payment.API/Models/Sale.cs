namespace Payment.API.Models;

public class Sale
{
    public DateTime Date { get; private set; }
    public Guid Id { get; private set; }
    public List<Item> Items { get; private set; }
    public Salesperson Salesperson { get; private set; }
    public SaleStatus Status { get; private set; }

    public Sale(DateTime date, Guid id, List<Item> items, Salesperson salesperson, SaleStatus status)
    {
        Date = date;
        Id = id;
        Items = items;
        Salesperson = salesperson;
        Status = status;
    }

    public bool CanChangeStatusTo(SaleStatus newStatus)
    {
        switch (Status)
        {
            case SaleStatus.WaitingForPayment:
                return newStatus == SaleStatus.PaymentApproved || newStatus == SaleStatus.Canceled;

            case SaleStatus.PaymentApproved:
                return newStatus == SaleStatus.SentToCarrier || newStatus == SaleStatus.Canceled;

            case SaleStatus.SentToCarrier:
                return newStatus == SaleStatus.Delivered;

            default:
                return false;
        }
    }

    public bool ChangeStatus(SaleStatus newStatus)
    {
        if (!CanChangeStatusTo(newStatus))
        {
            throw new InvalidOperationException($"Invalid status transition from {Status} to {newStatus}");
        }

        Status = newStatus;

        return true;
    }

}