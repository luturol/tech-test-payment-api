using Payment.API.Models;
using Payment.API.Models.DTOs.Requests;

namespace Payment.API.Interfaces;

public interface ISaleService
{
    SaleResponseDTO AddSale(SaleRequestDTO saleRequestDTO);
    List<SaleResponseDTO> GetAll();
    SaleResponseDTO? GetSale(Guid id);
    bool Remove(Guid id);
    bool UpdateSaleStatus(Guid id, SaleStatus status);
}