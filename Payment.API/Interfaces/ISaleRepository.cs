using Payment.API.Models;

namespace Payment.API.Interfaces;

public interface ISaleRepository
{
    bool AddSale(Sale sale);
    List<Sale> GetAll();
    Sale? GetSale(Guid id);
    Salesperson? GetSalesperson(string cpf);    
    bool RemoveSale(Sale sale);
    
}