using Payment.API.Interfaces;
using Payment.API.Models;

public class SaleRepository : ISaleRepository
{
    private readonly List<Sale> _saleList;

    public SaleRepository()
    {
        _saleList = new List<Sale>();
    }

    public bool AddSale(Sale sale)
    {
        if (!_saleList.Any(_ => _.Id == sale.Id))
        {
            _saleList.Add(sale);
            return true;
        }

        return false;
    }

    public List<Sale> GetAll() => _saleList;

    public Sale? GetSale(Guid id)
    {
        return _saleList.FirstOrDefault(_ => _.Id == id);
    }

    public Salesperson? GetSalesperson(string cpf)
    {
        return _saleList.FirstOrDefault(_ => _.Salesperson.CPF == cpf)?.Salesperson;
    }

    public bool RemoveSale(Sale sale)
    {
        return _saleList.Remove(sale);
    }
}