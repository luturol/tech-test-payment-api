using Payment.API.Models;
using Payment.API.Models.DTOs.Response;

namespace Payment.API.Extensions;

public static class ItemExtensions
{
    public static ItemResponsetDTO ToResponseDTO(this Item item)
    {
        return new ItemResponsetDTO
        {
            Name = item.Name,
            Price= item.Price
        };
    }
}