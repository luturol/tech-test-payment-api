using Payment.API.Models;
using Payment.API.Models.DTOs.Requests;

namespace Payment.API.Extensions;

public static class ItemRequestDTOExtensions
{
    public static Item ToItem(this ItemRequestDTO itemRequestDTO)
    {
        return new Item
        {
            Name = itemRequestDTO.Name,
            Price = itemRequestDTO.Price,
        };
    }
}