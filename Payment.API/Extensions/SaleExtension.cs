using Payment.API.Models;
using Payment.API.Models.DTOs.Requests;

namespace Payment.API.Extensions;

public static class SaleExtensions
{
    public static SaleResponseDTO ToResponseDTO(this Sale sale)
    {
        return new SaleResponseDTO
        {
            Date = sale.Date,
            Id = sale.Id,
            Items = sale.Items.Select(_ => _.ToResponseDTO()).ToList(),
            Salesperson = sale.Salesperson.ToResponseDTO(),
            Status= sale.Status
        };
    }
}