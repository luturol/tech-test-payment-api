using Payment.API.Models;
using Payment.API.Models.DTOs.Response;

namespace Payment.API.Extensions;

public static class SalespersonExtensions
{
    public static SalespersonResponseDTO ToResponseDTO(this Salesperson salesperson)
    {
        return new SalespersonResponseDTO
        {
            CPF = salesperson.CPF,
            Email = salesperson.Email,
            Id = salesperson.Id,
            Name = salesperson.Name,
            Phone = salesperson.Phone
        };
    }
}