using FluentValidation;
using Microsoft.AspNetCore.Mvc;
using Payment.API.Interfaces;
using Payment.API.Models;
using Payment.API.Models.DTOs.Requests;
using Payment.API.Validation;

namespace Payment.API.Controllers;

[Route("api/[controller]")]
[ApiController]
public class SaleController : ControllerBase
{
    private readonly ISaleService _saleService;

    public SaleController(ISaleService service)
    {
        _saleService = service;
    }

    [HttpGet]
    public IActionResult GetAllSales()
    {
        return Ok(_saleService.GetAll());
    }

    [HttpGet("{id}")]
    public IActionResult GetSaleById(Guid id)
    {
        var sale = _saleService.GetSale(id);

        if (sale == null) return NotFound();

        return Ok(sale);
    }

    [HttpPost]
    public IActionResult CreateSale([FromBody] SaleRequestDTO saleRequest)
    {
        try
        {
            new SaleRequestDTOValidation().ValidateAndThrow(saleRequest);
            var response = _saleService.AddSale(saleRequest);

            return CreatedAtAction(nameof(GetSaleById), new { id = response.Id }, response);
        }
        catch (Exception ex)
        {
            return BadRequest(ex.Message);
        }
    }

    [HttpPut("{id}")]
    public IActionResult UpdateSale(Guid id, [FromBody] SaleStatus saleStatus)
    {
        try
        {
            var response = _saleService.UpdateSaleStatus(id, saleStatus);

            return NoContent();
        }
        catch (NotFoundException ex)
        {
            return NotFound();
        }
        catch (InvalidOperationException ex)
        {
            return BadRequest(ex.Message);
        }
        catch (Exception ex)
        {
            return StatusCode(500, ex.Message);
        }
    }

    [HttpDelete("{id}")]
    public IActionResult DeleteSale(Guid id)
    {
        try
        {
            _saleService.Remove(id);

            return NoContent();
        }
        catch (NotFoundException ex)
        {
            return BadRequest(ex.Message);
        }
        catch (Exception ex)
        {
            return StatusCode(500, ex.Message);
        }
    }
}