using Payment.API.Extensions;
using Payment.API.Interfaces;
using Payment.API.Models;
using Payment.API.Models.DTOs.Requests;

public class SaleService : ISaleService
{
    private readonly ISaleRepository _saleRepository;

    public SaleService(ISaleRepository saleRepository)
    {
        _saleRepository = saleRepository;
    }

    public SaleResponseDTO AddSale(SaleRequestDTO saleRequestDTO)
    {
        var salePerson = _saleRepository.GetSalesperson(saleRequestDTO.Salesperson.CPF);
        if (salePerson is null)
            salePerson = new Salesperson
            {
                CPF = saleRequestDTO.Salesperson.CPF,
                Email = saleRequestDTO.Salesperson.Email,
                Name = saleRequestDTO.Salesperson.Name,
                Phone = saleRequestDTO.Salesperson.Phone,
                Id = Guid.NewGuid()
            };

        var sale = new Sale(saleRequestDTO.Date, Guid.NewGuid(), saleRequestDTO.Items.Select(e => e.ToItem()).ToList(), salePerson, SaleStatus.WaitingForPayment);

        _saleRepository.AddSale(sale);

        return sale.ToResponseDTO();
    }

    public List<SaleResponseDTO> GetAll() => _saleRepository.GetAll().Select(e => e.ToResponseDTO()).ToList();

    public SaleResponseDTO? GetSale(Guid id)
    {
        return _saleRepository.GetSale(id)?.ToResponseDTO();
    }

    public bool Remove(Guid id)
    {
        var sale = _saleRepository.GetSale(id);
        if(sale is null) throw new NotFoundException();

        return _saleRepository.RemoveSale(sale);
    }

    public bool UpdateSaleStatus(Guid id, SaleStatus status)
    {
        var sale = _saleRepository.GetSale(id);
        if(sale is null) throw new NotFoundException();

        return sale.ChangeStatus(status);
    }
}