using Payment.API.Models;

namespace Payment.API.Tests.Models;

public class SaleTests
{
    [Theory]
    [InlineData(SaleStatus.WaitingForPayment, SaleStatus.PaymentApproved)]
    [InlineData(SaleStatus.WaitingForPayment, SaleStatus.Canceled)]
    [InlineData(SaleStatus.PaymentApproved, SaleStatus.Canceled)]
    [InlineData(SaleStatus.PaymentApproved, SaleStatus.SentToCarrier)]
    [InlineData(SaleStatus.SentToCarrier, SaleStatus.Delivered)]
    public void Sale_Should_Change_Status(SaleStatus saleStatus, SaleStatus expected)
    {
        //Arrange
        var sale = new Sale(DateTime.Now, Guid.NewGuid(),
        new List<Item> { new Item { Name = "Test", Price = 10 } },
        new Salesperson
        {
            Name = "Salesperson",
            CPF = "123123123",
            Email = "salesperson@test.com",
            Phone = "12312323",
            Id = Guid.NewGuid()
        }, saleStatus);

        //Act
        sale.ChangeStatus(expected);

        //Assert
        Assert.Equal(expected, sale.Status);
    }

    [Theory]
    [InlineData(SaleStatus.WaitingForPayment, SaleStatus.SentToCarrier)]
    [InlineData(SaleStatus.WaitingForPayment, SaleStatus.Delivered)]
    [InlineData(SaleStatus.PaymentApproved, SaleStatus.WaitingForPayment)]
    [InlineData(SaleStatus.PaymentApproved, SaleStatus.Delivered)]
    [InlineData(SaleStatus.SentToCarrier, SaleStatus.PaymentApproved)]
    [InlineData(SaleStatus.SentToCarrier, SaleStatus.WaitingForPayment)]
    public void Sale_Should_Not_Change_Status(SaleStatus saleStatus, SaleStatus expected)
    {
        //Arrange
        var sale = new Sale(DateTime.Now, Guid.NewGuid(),
        new List<Item> { new Item { Name = "Test", Price = 10 } },
        new Salesperson
        {
            Name = "Salesperson",
            CPF = "123123123",
            Email = "salesperson@test.com",
            Phone = "12312323",
            Id = Guid.NewGuid()
        }, saleStatus);

        //Act & Assert
        Assert.Throws<InvalidOperationException>(() => sale.ChangeStatus(expected));
    }
}