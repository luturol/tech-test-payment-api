using Payment.API.Models.DTOs.Requests;

namespace Payment.API.Tests.Services;

public class SaleServiceTests
{    
    private SaleService _saleService;

    public SaleServiceTests()
    {
        var repository = new SaleRepository();
        _saleService = new SaleService(repository);
    }

    [Fact]
    public void Add_Should_Return_Success()
    {
        //Arrange
        var sale = new SaleRequestDTO()
        {
            Date = DateTime.Now,
            Items = new List<ItemRequestDTO>
            {
                new ItemRequestDTO{
                    Name = "test item",
                    Price = 10
                }
            },
            Salesperson = new SalespersonRequestDTO
            {
                CPF = "12312312312",
                Email = "salesperson@test.com",
                Name = "Teste",
                Phone = "123123"
            }
        };

        //Act
        var response = _saleService.AddSale(sale);

        //Assert
        Assert.NotNull(response);
        Assert.NotNull(response.Salesperson);
        Assert.NotNull(response.Id);
        Assert.NotNull(response.Salesperson.Id);
    }

    [Fact]
    public void Add_Should_Not_Create_Different_Salesperson()
    {
        //Arrange
        var sale = new SaleRequestDTO()
        {
            Date = DateTime.Now,
            Items = new List<ItemRequestDTO>
            {
                new ItemRequestDTO{
                    Name = "test item",
                    Price = 10
                }
            },
            Salesperson = new SalespersonRequestDTO
            {
                CPF = "12312312312",
                Email = "salesperson@test.com",
                Name = "Teste",
                Phone = "123123"
            }
        };

        //Act
        var response = _saleService.AddSale(sale);
        var response2 = _saleService.AddSale(sale);

        //Assert
        Assert.NotNull(response);
        Assert.NotNull(response.Salesperson);
        Assert.NotNull(response2);
        Assert.NotNull(response.Salesperson);
        Assert.Equal(response2.Salesperson.Id, response.Salesperson.Id);
    }

    [Fact]
    public void Remove_Should_Delete_Sale()
    {
        //Arrange
        var sale = new SaleRequestDTO()
        {
            Date = DateTime.Now,
            Items = new List<ItemRequestDTO>
            {
                new ItemRequestDTO{
                    Name = "test item",
                    Price = 10
                }
            },
            Salesperson = new SalespersonRequestDTO
            {
                CPF = "12312312312",
                Email = "salesperson@test.com",
                Name = "Teste",
                Phone = "123123"
            }
        };
        
        var response = _saleService.AddSale(sale);
        Assert.NotNull(response);

        //Act
        var act = _saleService.Remove(response.Id);
        var removedSale = _saleService.GetSale(response.Id);

        //Assert
        Assert.True(act);
        Assert.Null(removedSale);
    }
}